import React from 'react'
import Footer from './FooterComponent/FooterComponent'
import Icon from './Icon/Icon'

export function FooterContainer() {
    return (
        <Footer>
            <Footer.Wrapper>
            <Footer.Row>
                <Footer.Column>
                <Footer.Title>About Us</Footer.Title>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                </Footer.Column>
                <Footer.Column>
                <Footer.Title>Services</Footer.Title>
                    <Footer.Link href="#">Ezerx</Footer.Link>
                    <Footer.Link href="#">Ezerx 
                    </Footer.Link>
                    <Footer.Link href="#">Ezerx</Footer.Link>
                    <Footer.Link href="#">Ezerx</Footer.Link>
                </Footer.Column>
              
                <Footer.Column>
                <Footer.Title>Contact Us</Footer.Title>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                    <Footer.Link href="#">Ezerx </Footer.Link>
                </Footer.Column>
                <Footer.Column>
                <Footer.Title>SOCIAL</Footer.Title>
                    <Footer.Link href="#"><Icon className="fab fa-facebook-f" /> facebook </Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-instagram" /> instagram</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-youtube" />Youtube</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-twitter" />Twitter </Footer.Link>
                </Footer.Column>
            </Footer.Row>
            </Footer.Wrapper>
        </Footer>
    )
}
export default FooterContainer;