import React from 'react'
import { Icon } from './IconStyle'

export default function Icons({className}) {
    return <Icon className={className} />
}