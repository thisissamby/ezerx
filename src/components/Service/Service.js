import React from 'react';
import Icon1 from '../../images/service-society.svg';
import Icon2 from '../../images/svg-2.svg';
import Icon3 from '../../images/svg-3.svg';

import {
    ServicesContainer,
    ServicesH1 ,
    ServicesWrapper,
    ServicesCard,
    ServicesIcon,
    ServicesH2,
    ServicesP
} from './ServiceElement';

export const Service = () => {
    return (
        <ServicesContainer id="services">
            {/* <ServicesH1>Our Services</ServicesH1> */}
            <ServicesWrapper>
                <ServicesCard bgColor={true} >
                    <ServicesIcon src={Icon1}/>
                    <ServicesH2>SERVICE TO SOCIETY</ServicesH2>
                    <ServicesP>
                    Providing low-cost, highly advanced and reliable medical solutions to mass problems. 
                    </ServicesP>
                </ServicesCard>
                <ServicesCard bgColor={true}>
                    <ServicesIcon src={Icon2}/>
                    <ServicesH2 >CREATING AWARENESS</ServicesH2>
                    <ServicesP >
                    Using innovation technology to promote knowledge about common health issues.
                    </ServicesP>
                </ServicesCard>
                <ServicesCard bgColor={true}>
                    <ServicesIcon src={Icon3}/>
                    <ServicesH2>RURAL UPLIFTMENT</ServicesH2>
                    <ServicesP>
                    Making diagnosis easily accessible and creating employment opportunities.
                    </ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default Service
