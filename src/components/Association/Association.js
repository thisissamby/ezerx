
import React, { Component } from 'react'
import Carousel from "react-elastic-carousel";
import "./Association.css";
import styled from "styled-components";
import "./Association.css";
import "../../images/IOCL.png"

const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 4 },
  ];
class Association extends Component {

    render () {
        // const {imgurl} = this.state ;
        return(
            <>
            <h1 style={{justifyContent:"center" ,
                display : "flex",
                alignItems : 'center',
                position : 'relative',
                paddingTop : '25vh',
                color: 'black' ,
                fontFamily : 'Trocchi , sans-seriff',
                fontSize: '46px',
                fontWeight : 'bold',
                lineHeight :' 46px',
                textTransform : 'uppercase'
                          
                }}
            className="heading-text" > 
            Our Associates 
            </h1>
            <div className="association-container">
            <Carousel breakPoints={breakPoints} enableAutoPlay={true}>
            {/* {imgurl.map((url)=>{
                <Item key={url}><img src={url}/></Item>
            })} */}
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2020/10/technology-development-board-150x150.png" alt="IOCL-LOGO"/></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2020/10/birac-150x150.png" alt="IOCL-LOGO"/></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2020/10/villgrow-logo-150x150.png" alt="IOCL-LOGO"/></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/assocham-india-logo-feature-150x150.png" alt="IOCL-LOGO"/></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2020/10/technology-development-board-150x150.png" alt="IOCL-LOGO"/></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2020/11/KIIT-TBI_Logo_-e1605158722238-150x150.png" alt="KIIT-TBI LOGO"/></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2020/10/nrdc2-150x150.png" alt="logo" /></Item>
            <Item><img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Indian_Oil_Corporation-Logo.wine_-150x150.png" alt="logo" /></Item>
            </Carousel>
            </div>
            </>
        )
    }
} 

const Item = styled.div`

display: flex;
align-items: center;
height: 150px;
width: 150px;
background-color: white;
color: #fff;
margin: 0 15px;
font-size: 4em;
justify-content: center;

`;

export default Association


