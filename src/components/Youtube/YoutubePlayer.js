import React ,{ Component} from 'react'
import YouTube from 'react-youtube';



 class YoutubePlayer extends Component {
    
    
     VideoOnReady(event) {
        // access to player in all event handlers via event.target
        event.target.playVideoAt(50);
        // console.log(event.target);
      }
     VideoOnPlay(event) {
        // access to player in all event handlers via event.target
        // event.target.playVideoAt(50);
        const player = event.target;
        // console.log(player.getCurrentTime());
      }
    

    render() {
      const opts = {
        height: '670',
        width: '1000',
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
          autoplay: 1,
        },
      };
      
      const {videoId} =  this.props;

      return ( 
      <YouTube 
      videoId={videoId}
      opts={opts} 
      onReady={this.VideoOnReady}
      onPlay={this.videoOnPlay}
      
       />
       )
    }
  }

  export default YoutubePlayer;