import React from 'react';
import "./EzecheckLanding.css";
import Button from "../Navbartwo/Button"
import {AiOutlineCheck as Icon1} from "react-icons/ai"
import { IoTrendingUpOutline } from 'react-icons/io5';

export const EzecheckLanding = () => {
    return (
       <>
        <div className="super-container">
        <div className="child-container">
        
        {/* Image Container */}
        <div className="imgBx">
        <img src="https://ezerx.in/wp-content/uploads/2021/03/Untitled-design-2021-03-26T130008.076.png" alt="Nike Air Max 270" alt="Ezecheck Product Page"/>
        </div>
        
        <div className="details">
            <div className="content">
            <h2>EZE CHECK <br></br>
                    <span>EZE CARE</span>
                </h2>
                <p>
                    Featuring soft foam cushioning and lightweight, woven fabric in the upper, the Jordan Proto-Lyte is
                    made for all-day, bouncy comfort.
                    Lightweight Breathability: Lightweight woven fabric with real or synthetic leather provides
                    breathable support.<br></br> <br></br>
                    Cushioned Comfort: A full-length foam midsole delivers lightweight, plush cushioning.
                    Secure Traction: Exaggerated herringbone-pattern outsole offers traction on a variety of surfaces.
                </p>

               <ul className="unordered-list">
                   <li className="list-item "><Icon1/>Easy-to-use</li>
                   <li className="list-item"><Icon1/>Quick Results</li>
                   <li className="list-item"><Icon1/>Non-Invasive</li>
               </ul>
                <Button to="/about" primary={true} className="button-landing ml-5">About Us</Button>
            </div>
        </div>

        </div>
        </div>
       </>
    )
}

export default EzecheckLanding
