import React from 'react';
import EzecheckLanding from "./EzecheckLanding";
import EzecheckBody from "./EzecheckBody"
import EzecheckService from "./EzecheckService"

const Ezecheck = () => {
    return (
        <>
        <EzecheckLanding/>
        <EzecheckBody/>
        <EzecheckService/>
        <EzecheckService/>
        </>
    )
}

export default Ezecheck
