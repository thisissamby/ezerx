import React from 'react'
import ServiceElement from "../Service/ServiceElement"
import {
    ServicesContainer,
    ServicesH1 ,
    ServicesWrapper,
    ServicesCard,
    ServicesIcon,
    ServicesH2,
    ServicesP
} from "../Service/ServiceElement"
import Icon1 from "../../images/doctor.png"
import {AiOutlineUserSwitch as Icon} from "react-icons/ai";
export const EzecheckService = () => {
    return (
        <ServicesContainer id="services">
            {/* <ServicesH1>Our Services</ServicesH1> */}
            <ServicesWrapper>
                <ServicesCard bgColor={true} >
                    <Icon size="3x" color="red"/>
                    <ServicesH2>SERVICE TO SOCIETY</ServicesH2>
                    <ServicesP>
                    Providing low-cost, highly advanced and reliable medical solutions to mass problems. 
                    </ServicesP>
                </ServicesCard>
                <ServicesCard bgColor={true}>
                <Icon size="3x" color="red"/>
                    <ServicesH2 >CREATING AWARENESS</ServicesH2>
                    <ServicesP >
                    Using innovation technology to promote knowledge about common health issues.
                    </ServicesP>
                </ServicesCard>
                <ServicesCard bgColor={true}>
                <Icon size="3x" color="red"/>
                    <ServicesH2>RURAL UPLIFTMENT</ServicesH2>
                    <ServicesP>
                    Making diagnosis easily accessible and creating employment opportunities.
                    </ServicesP>
                </ServicesCard>
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default EzecheckService
