import React from 'react' ;
import svg1 from "../../images/svg-doctors-1.svg" ;
import {
    InfoContainer ,
    InfoWrapper , 
    InfoRow , 
    Column1 , 
    Column2 , 
    TextWrapper , 
    TopLine , 
    Heading , 
    Subtitle , 
    BtnWrap ,
    ImgWrap , 
    Img
} from "./ProdElement" ; 
import Button from "../Navbartwo/Button" ;



export const InfoSection = ({
    id , 
    lightBg , 
    lightText , 
    lightTextDesc,
    topLine,
    headLine,
    description,
    buttonLabel,
    imgStart,
    img,
    alt,
    dark,
    primary,
    darkText
}) => {
    console.log(
        id ,
        lightBg ,
        lightText , 
        lightTextDesc,
        topLine,
        headLine,
        description,
        buttonLabel,
        imgStart,
        img,
        alt,
        dark,
        primary,
        darkText
        );
    return (
        <>
        <InfoContainer lightBg={lightBg} id={id}>
            <InfoWrapper>
            <InfoRow imgStart = {imgStart}>
                <Column1>
                <TextWrapper>
                    <TopLine>{topLine}</TopLine>
                    <Heading lightText={lightText}>{headLine}</Heading>
                    <Subtitle darkText={darkText}>{description}</Subtitle>
                    <BtnWrap>
                        <Button to="home" primary="true" big="true">{buttonLabel}</Button>
                        
                    </BtnWrap>
                </TextWrapper>
                </Column1>
                <Column2>
                    <ImgWrap>
                    <Img src={svg1} alt={alt} />
                    </ImgWrap>
                </Column2>
            </InfoRow>
            </InfoWrapper>
        </InfoContainer>
        </>
    )
}

export default InfoSection
