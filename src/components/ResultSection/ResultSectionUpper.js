import React from 'react'
import "./ResultSectionUpper.css"
export const ResultSectionUpper = () => {
    return (
        <>
        <div className="container-fluid">
        <h2 className="h2class text-danger">Socio-Economic impact created in last 6 months</h2>
        <div className="row">
        <div className="div1 col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <h3 className="text-dark">Blood Saved:<h3 className="text-danger">45Ltr</h3></h3>
            <img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Untitled-design-2021-03-01T163701.794.png" className="img-fluid img-center img-responsive img-icons"/>
        </div>
        <div className="div2 col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Untitled-design-2021-03-01T163741.801.png" className="img-fluid img-center img-responsive img-icons"/>
            <h3 className="text-dark">Total Tests:<h3 className="text-danger">22,000</h3></h3>
        </div>
        </div>
        </div>
        </>
    )
}

export default ResultSectionUpper
