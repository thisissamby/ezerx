import React from 'react'
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css"


export const ResultSection = () => {
    return (
        <div className="container-fluid">
        <div className="px-lg-5">
             <h2 className="text-bold text-dark">DISEASES PREFILLED</h2>
            <div className="row">
            <div className="col-xl-3 col-lg-4 col-md-6 mb-4 ">
                 <img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Untitled-design-99.png" className="img-fluid img-center img-responsive img-icons ml-5"/>
                 <h3 className="h3class text-bold text-center text-danger"> Anemia : 21,780</h3>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6 mb-4  ">
                 <img src="https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Untitled-design-100.png "  className="img-fluid img-center img-responsive img-icons ml-5"/>
                 <h3 className="h3class text-bold text-center text-danger"> Liver Problems : 1,970</h3>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6 mb-4 ">
                 <img src=" https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Untitled-design-2021-03-01T165140.736.png"  className="img-fluid img-center img-responsive img-icons ml-5"/>
                 <h3 className="h3class text-bold text-center text-danger"> Lungs Problem : 972</h3>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6 mb-4 ">
                 <img src=" https://i0.wp.com/ezerx.in/wp-content/uploads/2021/03/Untitled-design-2021-03-12T135126.482.png"  className="img-fluid img-center img-responsive img-icons ml-5"/>
                 <h3 className="h3class text-bold text-center text-danger"> Kidney: 275</h3>
            </div>
            </div>
        </div>
        </div>
    )
}

export default ResultSection
