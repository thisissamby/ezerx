import React from 'react'
import ResultSection from "./ResultSection";
import ResultSectionMiddle from "./ResultSectionMiddle";
import ResultSectionUpper from "./ResultSectionUpper";

export const Result = () => {
    return (
       <>
        <ResultSectionUpper/>
        <ResultSectionMiddle/>
        <ResultSection/>
       </>
    )
}

export default Result
