import React from 'react'

const Image = () => {
    return (
        <>
        <div className="container-fluid">
        <div className="px-lg-5">
        <div className="row">
        {/* Gallery Item */}
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm">
        <img src="https://res.cloudinary.com/mhmd/image/upload/v1556294929/matthew-hamilton-351641-unsplash_zmvozs.jpg" alt="" className="img-fluid card-img-top"/>
        <div className="p-4">
            <h5> <a href="#" className="text-dark">TATA SOCIAL ENTERPRISE CHALLENGE</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC). </p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-danger px-3 rounded-pill font-weight-normal">TATA</div>
            </div>
        </div>
        </div>
        </div>

        {/* Gallery Item */}
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294927/cody-davis-253928-unsplash_vfcdcl.jpg" alt="" className="img-fluid card-img-top"/>
          <div className="p-4">
            <h5> <a href="#" className="text-dark">PFID</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC).</p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-primary px-3 rounded-pill font-weight-normal">AMJJ</div>
            </div>
          </div>
        </div>
      </div>
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294929/matthew-hamilton-351641-unsplash_zmvozs.jpg" alt="" className="img-fluid card-img-top"/>
        <div className="p-4">
            <h5> <a href="#" className="text-dark">TATA SOCIAL ENTERPRISE CHALLENGE</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC). </p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-danger px-3 rounded-pill font-weight-normal">TATA</div>
            </div>
        </div>
        </div>
        </div>

        {/* Gallery Item */}
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294927/cody-davis-253928-unsplash_vfcdcl.jpg" alt="" className="img-fluid card-img-top"/>
          <div className="p-4">
            <h5> <a href="#" className="text-dark">PFID</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC).</p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-primary px-3 rounded-pill font-weight-normal">AMJJ</div>
            </div>
          </div>
        </div>
      </div>
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294929/matthew-hamilton-351641-unsplash_zmvozs.jpg" alt="" className="img-fluid card-img-top"/>
        <div className="p-4">
            <h5> <a href="#" class="text-dark">TATA SOCIAL ENTERPRISE CHALLENGE</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC). </p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-danger px-3 rounded-pill font-weight-normal">TATA</div>
            </div>
        </div>
        </div>
        </div>

        {/* Gallery Item */}
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294927/cody-davis-253928-unsplash_vfcdcl.jpg" alt="" class="img-fluid card-img-top"/>
          <div className="p-4">
            <h5> <a href="#" className="text-dark">PFID</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC).</p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-primary px-3 rounded-pill font-weight-normal">AMJJ</div>
            </div>
          </div>
        </div>
      </div>
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294929/matthew-hamilton-351641-unsplash_zmvozs.jpg" alt="" className="img-fluid card-img-top"/>
        <div className="p-4">
            <h5> <a href="#" class="text-dark">TATA SOCIAL ENTERPRISE CHALLENGE</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC). </p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-danger px-3 rounded-pill font-weight-normal">TATA</div>
            </div>
        </div>
        </div>
        </div>

        {/* Gallery Item */}
        <div className="col-xl-3 col-lg-4 col-md-6 mb-4 shadow-sm">
        <div className="bg-white rounded shadow-sm"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556294927/cody-davis-253928-unsplash_vfcdcl.jpg" alt="" className="img-fluid card-img-top"/>
          <div className="p-4">
            <h5> <a href="#" className="text-dark">PFID</a></h5>
            <p className="small text-muted mb-0">EzeRx wins the second position in the 7th edition of the Tata Social Enterprise Challenge 2018-19 (TSEC), a joint initiative of the Tata group and Indian Institute of Management Calcutta (IIMC).</p>
            <div className="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">
              <p className="small mb-0"><i className="fa fa-picture-o mr-2"></i><span className="font-weight-bold">EZERX</span></p>
              <div className="badge badge-primary px-3 rounded-pill font-weight-normal">AMJJ</div>
            </div>
          </div>
        </div>
      </div>


        </div>
        </div>
        </div>
        </>
    )
}

export default Image
