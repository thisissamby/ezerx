import React from 'react'
import ImagesGallery from "./ImagesGallery"
import Image from "./Image"


export const Media = () => {
    return (
        <>
        <ImagesGallery/>
        <Image/>
        </>
       
    )
}

export default Media
