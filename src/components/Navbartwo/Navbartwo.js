import React from 'react'
import styled , {css } from "styled-components/macro"
import {Link} from "react-router-dom"
import menuData from "./data/menuData"
import Button from "./Button"
import Bars from "../../images/bars.svg"

export const NavbarTwo = () => {
    return (
        <Navbar>
            <Logo to="/">Ezerx</Logo>
            <MenuBars/>
            <NavMenu>
                 {menuData.map((item , index )=>(
                     <NavmenuLinks to={item.Link} key={index}>
                        {item.title}
                     </NavmenuLinks>
                    
                 ))}
            </NavMenu>
            <NavBtn>
                <Button  to="/bookslot" primary="true" > Book Slot </Button>
            </NavBtn>
        </Navbar>
    )
}

const Navbar = styled.div`
    height: 80px;
    background : white;
    display : flex;
    justify-content : space-between ;
    padding : 1rem 2rem;
    z-index : 100;
    position : sticky ; 
    width : 100%;
    /* background : red ; */
    font-family: 'Montserrat' , sans-serif ;
`;

const NavLink = css`
    color : black;
    display : flex ;
    cursor: pointer;
    display : flex;
    align-items :center ;
    padding : 0 1rem ;
    height : 100%;
    text-decoration : none ;
`;

const Logo = styled(Link)`
    ${NavLink} ;
    
`;


const MenuBars = styled.i`
    display : none ;

    @media screen and (max-width : 768px) {
        display : block ;
        background-image : url(${Bars});
        background-size : contain ;
        height : 40px ;
        width : 40px ; 
        cursor : pointer ; 
        position : absolute ;
        right : 0 ; 
        transform :  translate(-50% , -25%);

    }
`;

const NavMenu = styled.div`
    display : flex ;
    align-items : center; 
    margin-right : -8px;

    @media screen and (max-width : 768px) {
        display : none ;
    }
`;

const NavmenuLinks =styled(Link)`
    ${NavLink}
` 
const NavBtn = styled.div`
    display : flex ; 
    align-items : center ; 
    margin-right : 24px;

    @media screen and (max-width : 768px) {
        display : none ;
    }
`

export default NavbarTwo