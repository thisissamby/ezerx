export const menuData = [
    {title : 'Home' , link : '/home'} , 
    {title : 'About Us' , link : '/about'} , 
    {title : 'EzeCheck' , link : '/product'} , 
    {title : 'Aarogya' , link : '/aarogya'} , 
    {title : 'Media' , link : '/media'} , 
    {title : 'Awards' , link : '/awards'} , 
    {title : 'Contact Us' , link : '/contactus'} , 
]

export default menuData