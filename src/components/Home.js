import React from 'react';
import {Youtube}  from "./Youtube/Youtube";
import Cards from "./TestimonialCard/Cards";
import Association from "./Association/Association"
import Hero from "./Hero/Hero" ;
import SliderData from "./Hero/SliderData";
import cardData from "./TestimonialCard/cardData";
import { useMediaQuery } from 'react-responsive';
import Media from 'react-media';
import InfoSection from "./Product/Prod";
import { homeObjOne , homeObjTwo } from './Product/ProductData';
import Service from "./Service/Service" ;
import Result from "./ResultSection/Result"

const images = [
    'https://ezerx.in/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-30-at-12.05.57-PM.jpeg',
    'https://ezerx.in/wp-content/uploads/2020/12/WhatsApp-Image-2020-12-30-at-12.07.39-PM.jpeg',
    'https://images.unsplash.com/photo-1448630360428-65456885c650?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2094&q=80',
    'https://images.unsplash.com/photo-1534161308652-fdfcf10f62c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2174&q=80'
  ]

export function Home() {
    return (
        <>
        <Hero SliderData= {SliderData}/>
        {/* <Media query="(min-width:600px)">
            {matches => {
                return matches ? <Youtube/> : 'world' ;
            }}
        </Media> */}
        {/* <Youtube /> */}
        <InfoSection {...homeObjOne}/>
        <Service/>
        <Result/>
        <InfoSection {...homeObjTwo}/>
        <Cards cardData={cardData}/>
        {/* <CardsTwo/> */}
        <Association/>
        
        </>

    )
}
