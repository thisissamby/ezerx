import React from 'react'
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "./Foot.css"

export const Foot = () => {
    return (
        <footer className="kilimanjaro_area">
            {/* Top Footer Area Start */}
            <div className="foo_top_header_one section_padding_100_70">
                <div className="container">
                    <div className="row">

                    {/* Column 1 */}
                <div className="col-12 col-md-6 col-lg-3">
                    
                    {/* Col -1 A About US */}
                    <div className="kilimanjaro_part">
                    <h5>About Us</h5>
                    <p>It includes rich features and content .It includes rich features and content .It includes rich features and content .</p>
                    <p>It includes rich features and content .It includes rich features and content .It includes rich features and content .</p>
                    </div>

                    {/* Col - 1 B Social Links */}

                    <div className="kilimanjaro_part m-top-15">
                        <h5>Social Links</h5>
                        <ul className="kilimanjaro_social_links"> 
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Facebook</a></li>
                        </ul>
                    </div>
                </div>
                    {/* Col 1 Ends */}

                {/* Column 2 */}

                <div className="col-12 col-md-6 col-lg-3">
                    {/* Column 2 A Tags*/}
                    <div className="kilimanjaro_part">
                    <h5>Tags Widget</h5>
                    <ul className=" kilimanjaro_widget">
                                <li><a href="#">Classy</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">One Page</a></li>
                                <li><a href="#">Multipurpose</a></li>
                                <li><a href="#">Minimal</a></li>
                                <li><a href="#">Classic</a></li>
                                <li><a href="#">Medical</a></li>
                            </ul>
                    </div>

                    {/* Column 2 B Links */}
                    <div className="kilimanjaro_part m-top-15">
                        <h5>Important Links</h5>
                        <ul className="kilimanjaro_links">
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Terms & Conditions</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>About Licences</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Help & Support</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Careers</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Privacy Policy</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Community & Forum</a></li>
                        </ul>
                    </div>
                </div>

                {/* Column 3 */}

                    <div className="col-12 col-md-6 col-lg-3">
                    <div className="kilimanjaro_part">

                    {/* <!--Col 3 A--> */}
                            <div className="kilimanjaro_blog_area">
                                <div className="kilimanjaro_thumb">
								<img className="img-fluid" src="https://3.bp.blogspot.com/--C1wpaf_S4M/W7V__10nRoI/AAAAAAAAK24/1NSfapuYSIY0f0wzXY9NgoH0FjQLT07YACKgBGAs/s1600/maxresdefault.jpg" alt="pic1"/>
                                </div>
                                <a href="#">Ezerx News Ezerx News</a>
                                <p className="kilimanjaro_date">14 Apr 2021</p>
                                <p>Ezerx is leading in health sector </p>
                            </div>
                    {/* Col 3 B */}
                            <div className="kilimanjaro_blog_area">
                                <div className="kilimanjaro_thumb">
								<img className="img-fluid" src="https://3.bp.blogspot.com/--C1wpaf_S4M/W7V__10nRoI/AAAAAAAAK24/1NSfapuYSIY0f0wzXY9NgoH0FjQLT07YACKgBGAs/s1600/maxresdefault.jpg" alt="pic2"/>
                                </div>
                                <a href="#">Ezerx News Ezerx News</a>
                                <p className="kilimanjaro_date">14 Apr 2021</p>
                                <p>Ezerx is leading in health sector </p>
                            </div>

                    {/* Col 3 C */}
                            <div className="kilimanjaro_blog_area">
                                <div className="kilimanjaro_thumb">
								<img className="img-fluid" src="https://3.bp.blogspot.com/--C1wpaf_S4M/W7V__10nRoI/AAAAAAAAK24/1NSfapuYSIY0f0wzXY9NgoH0FjQLT07YACKgBGAs/s1600/maxresdefault.jpg" alt="pic3"/>
                                </div>
                                <a href="#">Ezerx News Ezerx News</a>
                                <p className="kilimanjaro_date">14 Apr 2021</p>
                                <p>Ezerx is leading in health sector </p>
                            </div>


                    </div>
                    </div>
                
                {/* Col 4 */}
                    <div className="col-12 col-md-6 col-lg-3">
                        {/* Col 4 row -1  Quick Contact*/}
                        <div className="kilimanjaro_part">
                        <h5>Quick Contact</h5>

                        {/* Part - 1 */}
                        <div className="kilimanjaro_single_contact_info">
                            <h5>Phone:</h5>
                            <p>+255 789 54 50 50 <br></br> +23132128888</p>
                        </div>
                        {/* Part - 2 */}
                        <div className="kilimanjaro_single_contact_info">
                                <h5>Email:</h5>
                                <p>support@ezerx.in <br></br> ezerx@ezerx.in</p>
                        </div>

                        </div>
                        {/* Col 4 row -2 Latest works*/}
                        <div className="kilimanjaro_part">
                        <h5>Latest Works</h5>
                        <div className="kilimanjaro_works">
                            <a className="kilimanjaro_works_img" href="#"> <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgVFhUZGRgZGBwZGhgaGRoaGRgaHBohGRwdGRkhIS4mHB4rHxwcJjgmLC8zOjY1HCU7QDszPy40NTEBDAwMEA8QHxISHz8rJCw2MTY0NDQ/OjQ0NTY0PzY0NDo0MTY0MTY0MTQ0NDE0NjQ0NDQ0NjQ0NDQ0MTQ0NDQ0NP/AABEIALQBFwMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABQYDBAECBwj/xABLEAACAQIDAwkFBAYHBQkAAAABAgADEQQSIQUxQQYTFSJRVGFxkzJSgZHSFCOhsXKSssHR0zM0QmJ0grMkU3Oi8AclNWNkhLTh4v/EABkBAQEBAQEBAAAAAAAAAAAAAAABAgMEBf/EACoRAAICAQQBAwMFAQEAAAAAAAABAhFRAxIUITETQYEyYbEEIkJxkfAF/9oADAMBAAIRAxEAPwCkxET7p8MREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREARM2GwzucqIWPYBf59kYjDOhyupU9hFoIYYiIKIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAZFosQSFJA3kAkDzPCdJ6Zs6mEpIq7si/G6gk/GUTb+GFPEOqiy3DAeDAN+ZMHOM9zaLDyLUc25457fAKLfmZ25Z0gaKtbVWAB8GuCPwEwciKnVqL4qfmCP3Tf5VrfDP4Mp/5gP3wzl41ShTm04lo5G4NWLVGAOWyrfgTqT8rD4mDvJ7VZWLTiXLlfgl5sVQAGVgCRpcNpr22NpTYEZblYiIg0IiZKVMMbF1Qe82aw/VVj+EAxxNz7IneaXyr/wAqPsid5pfKv/Kk3Iu1mnE3Psid4pfKv/KmfDYRCle9ROpzeWpapl6zWIAyZvDVeEjkkVRbIyJMNs5DVooGXIaKu7glVKhnLNdwMvVW2ttbTLSwSHE0+omSopqBQ4dFORsy51JBCupG/da8nqIvpsg4k9hqVJ61BSKJYioagQtzVghKXud9w17cAPGatepSDUy602PXFQUS2TIQApBJtzgu50NtEvxhal+xXp0rsipzJDaGFRHWhnQFLl6pD5WZgGAAVS2ULlG7eW4WmL7IneKXyr/ypVJGHFmnE3Psid5pfKv/ACo+yJ3ml8q/8qXchtZqTiZcRTVSAro/igcAeBzov4THKnZGqOIiIAiIgCIiAIiIBzOJPcnNjpXDs5NlsoCm2p1udJobY2caFQqTcEZlbtU/v4QZUle33LtsCvnoUz2LkPmvV/ICVrllTtXVveQfgxH8JI8iavUdexgf1h/+Y5a4e6I/usVPkw/iv4wcY/t1GjX5EDrVfJfzaTu36WbD1R/dv+qQ37pB8iDrVHgp/allx1ube+7I/wCyYZmbrU/w8wl25GL9yx7XP7KykjdPQeTVHLh0/vXb5nT8LQddd1E0uWlS1FV95x+AJ/hKXLdy39il+k35CVGC6X0IREQdDmchb6AXJ0AG8nsAnWS/JWvTTGUHqkBFcEk7lNiFY9gDWN+FrzMnSbLFW0iSw3ILGMoZhTp5ty1HCsf8oBt5HWRW2dg4jCkCvTKg+y4IZG8mHHwNjJzljyVxjYirW5tq6OxKOnXIQm6LlGoyjTQW0vxleLV3NPDOz2DhUR83UZyF9k6jhp59pnCE5S7tUd5wiuqdmfYnJ/EYonmUzKDZnYhVU77FjvPgLnWSO0ORmNo02YBXTQtzTZrZdRmXQm3gDNzl9jTSZNn0SUoUaa5wNOcZhc5+0WINuJJ8JXNhbWqYSqtWkSLEZ03K68VYcdL2PCVS1Jrcqr2QahF7X5yafPtb2zbJk3n2L5sv6N9bSxbK5JY6qiuoFNBco1R8ntixKjUi4trYX8ZZm5OUW2qjAfcvR+15eFwQCLdmYq1vEyjcotsPjKzVHYlMx5tP7KJfqgDde288TMrUep1D5K4KHcvgz7W5O4rB2d1yruWojArqLWzDUXBI1AvNWvseomHTFNl5t3KLr1swzb1toOoZkwW3K1KhUwysDTqLYqwzBO0oP7JP/wB75PbW/wDBcJ/iW/KtNbpxq80Z2xd1iyqUab1qiqLu7sqi5uSTZRc/KbO2tkVMLVNGqAGyhuqbqVO4g2HEEfCTXIemtNq2NcdTDISt9zVWBVVB7d/6yzNtao2M2dTxLHNWwzmlWPEo2qsfIlfm0S1WtTb7ePkLTTjfv5+CA2PsepiS608t0Qu2ZrdUb7aG5kaDLt/2Z/0mJ/wzfmJSKfsjyE3GTc5RxRiUUoqWbO0RE6nMREQBERAEREAREQCzcisRZ3T3lDDzU6/gfwkjyvwWekKgGqG5/QOh+RsfnKtsbFc3WRjuDWPkwyn856O6AgqRcEEEcCDoYPPqftmpFN5F17VHT3kuPNT/AAYyz7Xw3OUXTiVuP0h1l/ECVPDUfs2NVWPVzWBPFGBCk/PXxBl5gzq9TUkUnkW/3zDtpn8GWWnbJ+4q29x/2TKvs/F06WNqHMMhZlDcBdgflcWvLFtPaVFaT3qIbowADKSxIsLAHxk9jU03NOsHn+Gol2VV3sQo8ybT07D0gqqo3KoUfAWlG5JKDiFvwViPO38Ly/Sk132kU3ltXu9NPdUsfNjYfs/jK1JPlJUzYmoewhR/lUA/jeRkiO8VUUhEATs1Mr7QI8wRKaOs2tn4CpXcU6SlmIJCi24C51OgmrNnZ5qB1NEsri7KVOVtBc2PkDpxkd10VVfZtYbauKwrFFrVaRXehJAHmjafhLltLaLV9mpja6KK9KunNuFy84BUXd4Wv4XS8i8Ny2xxUZlpVQGCBnpgtmI0HVI3+Uidt7VxWLZueYEUbnIoVUTXKdBvN9NST2TzPTlOStJfc9KnGKdO/sTPL7BGoybQpAtQrU0LEa5GUWIbs0AFzxBHZK3sTZVTFVVpUgTcjMw1VF4sx4aX85u8mtrYugzDDt1T1mVgDT7LtewXhrcTe21yrxxXmmKUVYa80oXMONnBP/KRKlqQWxf6YctOT3N94LBW5SUU2sig/cpR+yFuF2IJN+wMFUnwMpHKLYr4Ss1N1IXMSj/2XS/VIPbbeOE1sLs13XMijKDluWVRe17akcCJO4XlTjcKootldABlWqoYBeGVgdR8SNIjpy02nDvJXqRn1LrBGbN2DWr0qtdQFp0lLF2OVWtvVTxNr/lJzax/7lwp/wDUt+VWRe3tvYvEBVrtZGCutNQFUi/VNhqdRxJ3TFW2nXWnSwjKmWjVFRVIu2Y3OVzm1HXNxpDjqSpvJFKCtLBaca+HwWCoYXEUXqNWvXqKrZCDcWDnjYEC39yduSm0cC7vhKdCpSGJRkYtULqSFNrAnRt9jK1ttMViKrVqwTNYKQHRQoUbguckcT5kyM2ZTqGoppe0hFQG4GXKwINyRxtItC4u32/yX10n14X4LjyCwrUsRjKTe0lCoh81a1/I7/jKBTcZRqNw4iW5+UOMo13xZSkHqqKbMFzIctvdc2awHHhNrC8tMc9ytPD2G9jTyqDwGYuBfwlitSMnKruvcSlpyilfiykgztJjlLtPEV3X7QioyKQoVcoKk3vvIbdvEh56IttdnCSV9eDiIiUgiIgCIiAIiIBzPQuT+N52gpJuy9Ru243H4ixnnkzYaq4YBGZWay9UkE30A0gxOO9UTfLKurVlUb1WzHsubgfD98y7N2biqqAPWqJT7CzZmHgt93nJjZGwUpAMwz1N5ZtbE78o/fvkzIcZaqitsfb3IGnyUw4FjnbxzW/ACYa3JGkfZd188rD8hLJEtHP1ZZKgeStRSGSqtwbg2ZSD8LzjH7XxlHquFF9z5b38je1/hLhI3b2F5yg4tqBnXzXX8RcfGRm46u5pSR52WJJJNyTck7yeN52o0i7BVF2YgAdpM6SycjsJmdqhGiCy/pN/AX+cp6JS2psn9kbHSgo0DPbrMfyXsH5zNtbCLUpOrAaKWVuKkAkEGb0r3KvagpoaKnruLH+6h338Tu+cM8sN05lIBmbDVyjq671YMPgbzCJt7OwD13FOmAWIYgE2vlUsRftsJG6Vs9tW6RPYFFSo7XAp1Mhp3GgdyWT9Q5r+UicSppUBTbR6rlnvvCoxVQfNsx+AmnVruVWmzHKhNlItlJ3+N7xisQ1RizsWYgC+nAWG6SiJdm5UB+yLl3c63OeeUZM3ha9vGKV/stTN7POJzd/e1z5f8tr/AAnFelXwj5ScrMisV0YZWGYBlIsSOI4GZqmDrVVR61VKakdTnGCXXtSmik5fELY9sjkqs1td0KNJWwozvkArsQcrNc5BpYbp02yAnN0RchEvnO5w5zgrr7HZ8Z0xeCqJTFnSpRDXz02DqrkW62gZSQNzATihhq1dGK9dcOmYjTMELa5dLsAbm3AXtG5ebG13RKgoOZ61qzYdVpkjqI1zYk+8SbA8DrIGkhFQBr5g4zX33za38bzpVrM2XMb5VCroNFGoGm/eZn+8qB6xYE01UsTbN7QRdw6xuRvl8dkSfhG3trmOdq6VM+duK5M1/K9p12KgPPBmyqaDXaxNhmXWw3zdZqranG4TXfcC+vb9zvmBtkvSZ0OKwyNY03Uu5O/VT932gbuyZ3Lwa9OVHSvTSnhyFbOKrDrBcqpkNyLE3zG/ymDD4lOa5uqr5M5dWQgENYKdG0bS06NSdaLkOppmrzZtqGdVLBluL2tfXS86YbaFRFyq/VvfKVV1v25WBAml2Zaa8nfaGECZGVyyOt1JFiADYgjwPZNKbmOFRlSrUYMKgYJuFgjZSMoACi/Z2zUlRGmvJxERKBERAEREAREQBNvZLWr0if8AeJ+0JqTLhWs6HsZT8iDBH4PUpwBOZ3w7BXVjuDKT5Ai8jdI8MVbSJmhydYrcsAx4WvbzN5rUNh1GYq1lA3tvB7LS3gzmfL5Wp2fd4Gi66KhtHYzUlzg3Ub9LEePlIoiXbbLgUXvxUgeZ0H4ylT2fptSU4vcfO/W6UdKaUcHm+19nNQcqQcp1Vu1f4jcZbOSKAYcEb2difPd+QElMXhUqIVdcyn5jxB4GU7Gc9gnKo5yP1lJAIPDUHcw0/Cek57vUjt9y1bX2itCmWOp3KvvN/AbzPO69ZnZmY3ZjcmZMZjHqtmqMWI3bgB5AaCYJDpCCgvucSU5Pn7xyNPuMRqN4+4fcZFyT5P8A9I/+HxH/AMd5mf0s6w8o52mOcRcSu9jkrAaWrWvmt2OLt+kH8JxspQitiXAIpkLTU7nrEXTTiqAZz5KOMxbJxKoxR781UXJUsLkDerge8jWYeRHGNqYhWZadM3pUgVQ2ILsdXcg6gu3DgoUcJmn9P/Ub6+ol6tMPiKL1buqYKnWqX1LhKJcg9uYix8zK/icQ9RmqOczubk/kB2ADQDgABJ6tiUSvh85IR8HSp1CN4SpRyMfhmzfCQmNwjUmKOLEagjUMvBlPFSNQZIefg1qPGTLsrF83UDHVG6lRDuemxs6n4ajsIB4SToK+HGNVGKtSemqsN/VxFgfiOHjI7ZODzvdtKSWeq/BEBufNjuVd5JE2hiTUp46qRYuyPbszV81vhe3wifn8iF1+DFtGgrp9opKFQkLUQbqLndlHuNYlew3XgL9cB/V8V+hS/wBZJg2djTSbNlDKwKvTPsuh9pT2eB3ggHhJV8EEoYl0bNSenTam/G3PpdH7HU6EeR3EQ+ltfwZXbtfJACSXKX+uYn/jP+0ZGrJLlL/XMT/xn/amv5r+mZ/i/wCzZwuCqVcERTpu5GKBIRSxA5ki5tuE1egMV3at6b/wne/+w/8Aux/omRdz2n5yLdbrJp7aV4JnbOGenQwiOjIwWsSrAqRerpoZCyV2h/V8J5V/9WRU3DwZ1PPwIiJowIiIAiIgCIiAIiIB6fgquamje8in5gXmxIDkjjM9HIT1kNv8p1H7x8JPweGcak0TmxtshbU6h0GgbsHYf4ywVsUirmLADtv/ANXlDgsbAXNhuF9B5DhPJP8ASRlK06Pbo/8AoShHa1eCQ2ttM1jYaINw4k9pkfET0wgoKonj1NSWpJyk+xKly3rC9NOIzMfAHQfMg/KWTG4tKSl2NgPmTwA8TPOcfi2qu1Rt7HQe6OAHkJWdNCHe414iJT0iZcNiGQllNiVZDpfqupRh8mMxTJRqlWzAKbcGUMvxU6GRroqZjETc6Sf3KPoU/pnPSTe5R9Cn9MzbwWlk18RiGcqXN8qKi6AWVRZR8pIYXGV1o5rK9FHyZaiJURHYFuqGBK3F91hNfpFvco+hT+mbWH2soTK6Bg1Ql6agIjIUVRYqOqwdQwIG8SSuqo3Fq7swbSr12VFqdVWUVEpoqoliSoYIgAubHU6/OcLhqyO2GAs1QorLddbEOvW3DWxvebDbUV6lKq/WamjMVsQrVBUd0UdiAsnwFp3obTp85h6rKVNNrOLs+ZAcyG5Nyesy+QWZTaVUaaV3ZHVcE65SMrhzlVkdXDNp1bqT1usum/UTZqCvSpPSzLzbuodFdHCuuoDBSSjdXwvltw07Udpqow7KioadQu9NQ2VjdbOGYsb5RlsTpluN5mpXo0UAyVGc5ha6ZMq6+0b6tu3ab9Zbb8ozSXhnR8M6lwRrTvn1HVs4pnz6zAads4xNdnd3c3d2LMbWuTqdBJTF7XDtiuqlqjE07UkBP+0JUGcgXPVU3B3nfNLpFvco+hT+mai2+2iSSXSZg+0Nk5u/Uz57WHtZct7+UxTc6Rb/AHdH0Kf0x0i3uUfRp/TL3gz1kw1MQzIiE9VMwUWGmZsza8dZhmWviC5BIQW06iKg+IUC8xSokvIiIlIIiIAiIgCIiAIiIBt7Mx7UXDr5EcGHEGX/AGftKnWW6sL8UNgw8x++eazkG2oNj4QYnpqZ6radDVXdmW/mJ5g+Ic6F2I7CzH98x2g58dZPVgZG7R21Rog5mDN7qkFvj7vxnngY9p+c6gSdlWhH3ZvbU2m9dszaAeyo3L/E+M0onMtHbx0jiIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIBIdB4ruuI9Gp9MdB4ruuI9Gp9M+i7RafO5ksH0OHHJ86dB4ruuI9Gp9MdB4ruuI9Gp9M+i7RaOZLA4ccnzp0Hiu64j0an0x0Hiu64j0an0z6LtFo5ksDhxyfOnQeK7riPRqfTHQeK7riPRqfTPou0WjmSwOHHJ86dB4ruuI9Gp9MdB4ruuI9Gp9M+i7RaOZLA4ccnzp0Hiu64j0an0x0Hiu64j0an0z6LtIjaO1CjZFQls1O18oDhqiowS7C7AMPAXF98cyWBw45PC+g8V3XEejU+mOg8V3XEejU+me3DlCjBClN2zkADqggFqYJN2/wDNX5GdaXKIFQTRq5ubWoQqhlGa9lz5stzbiRvG6OZLA4ccnifQeK7riPRqfTHQeK7riPRqfTPcK+2wu+k4Cl898uZQi5r2zag2NjxteYsRyhAVitNwUdFqZgpy5qmRlsrEs1gxGW43eIjmSwOHHJ4p0Hiu64j0an0x0Hiu64j0an0z3jFbVRACVYg02qG2U5UW1yetrqwGl5q1OUSKWBpVLrfP/R9UgOxHt6nLTc6X3Ab9I5ksDhxyeI9B4ruuI9Gp9MdB4ruuI9Gp9M+gzVJzAKSQARe6qSb2Aa3hrvteRFDbFRyoFLV6IqLcsF1QMTny2yhiF97W9rRzJYHDjk8T6DxXdcR6NT6Y6DxXdcR6NT6Z7TV2+RlIRes6U1GY5s7otS9rezZ7eYHbplo7WqurMtIH7ui6gNck1GZXBFgAEyk77mx3aXcyWBw45PEeg8V3XEejU+mOg8V3XEejU+mezvygbrEU7qEV8+V7BCAQ5AW5VutYLcjKb24b+zNqc6xUoUIFwG3kiwqW8FZgvmDwtHMlgcOOTwnoPFd1xHo1PpjoPFd1xHo1Ppn0XaLRzJYHDjk+dOg8V3XEejU+mOg8V3XEejU+mfRdotHMlgcOOT506DxXdcR6NT6Y6DxXdcR6NT6Z9F2i0cyWBw45PnToPFd1xHo1PpjoPFd1xHo1Ppn0XaLRzJYHDjk+dOg8V3XEejU+mOg8V3XEejU+mfRdotHMlgcOOT506DxXdcR6NT6Yn0XaI5ksDhxycxETxnsEREAREQBERAEREATW5hC2YoubdmsL2Go184iAdej6Wv3Sa2B6i62tlvpwsLeQgYKne/NpfdfKt9Tc8O3XznEQDucFT16i6tmPVGrHTMdN9uM608FTGW1NBYWWyKMovey6aC+vnOIgHb7FT6v3aabuqumltNNNAB5CcrgqYFhTQC1rBVtaxFt27rN+se2IgHesoykEAgixBFwR2WmOngqYXKKaBSuUqFXKV1NrWtbU6eMRAOF2fStbmktlyWyLbLe+Xd7N+G6dG2dRO+lTN1sbouqi1l3btBp4CIgGfmF91dSp3DeLWPmLC3Zac08OoJYKAzWzEAXNt1zxiIBmiIgCIiAIiIAiIgCIiAIiIB//2Q=="/></a>
                        </div>
                        </div>
                    </div>
                    {/* Col 4  ends */}

                    </div>
                </div>
            </div>

            {/* Footer Bottom Area Start */}
            <div className=" kilimanjaro_bottom_header_one section_padding_50 text-center">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <p>© All Rights Reserved by <a href="#">Ezerx Health Technology<i class="fa fa-love"></i></a></p>
                    </div>
                </div>
            </div>
        </div>
        </footer>
    )
}

export default Foot
