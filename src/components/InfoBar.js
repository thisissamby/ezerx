import React from "react";
import "./InfoBar.css";
import {FiPhoneCall  } from "react-icons/fi" ;
import {BiEnvelope as Email  } from "react-icons/bi" ;
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faYoutube,
    faFacebook,
    faTwitter,
    faInstagram,
    faLinkedin
  } from "@fortawesome/free-brands-svg-icons";


export function InfoBar() {
  return (
    <div className="infobar">
      <div className="overlap-group border-1px-dove-gray">
        
        <div className="rectangle-2 ">
        Advanced, Affordable Health Care Made Hassle-Free
        </div>
        <div className="rectangle-5 ">
        <FiPhoneCall/>1800 833 8837
        </div>

        <div className="rectangle-4 ">
        <Email/> Support@ezerx.in
        </div>

        <div className="rectangle- ">
        <a href="https://www.youtube.com/c/jamesqquick"
        className="youtube social">
        <FontAwesomeIcon icon={faYoutube} size="2x" color="white" />
        </a>
        </div>

        <div className="rectangle- ">
        <a href="https://www.youtube.com/c/jamesqquick"
        className="youtube social">
        <FontAwesomeIcon icon={faFacebook} size="2x" color="white" />
        </a>
        </div>

        <div className="rectangle- ">
        <a href="https://www.youtube.com/c/jamesqquick"
        className="youtube social">
        <FontAwesomeIcon icon={faLinkedin} size="2x" color="white" />
        </a>
        </div>

        <div className="rectangle- ">
        <a href="https://www.youtube.com/c/jamesqquick"
        className="youtube social">
        <FontAwesomeIcon icon={faTwitter} size="2x" color="white" />
        </a>
        </div>

        <div className="rectangle- ">
        <a href="https://www.youtube.com/c/jamesqquick"
        className="youtube social">
        <FontAwesomeIcon icon={faInstagram} size="2x" color="white" />
        </a>
        </div>

      </div>
    </div>
  );
}