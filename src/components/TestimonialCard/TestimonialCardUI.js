import React from 'react';
import "./TestimonialCard.css";

function TestimonialCard(props) {
    return (
        <div className="card text-center shadow">
            <div className="overflow">
                <img src={props.imgsrc}  className="card-img-top" alt="image1"/>
            </div>
            <div className="card-body text-dark">
                <h4 className="card-title">
                Card title
                </h4>
                <p className="card-text text-secondary">
                    lorem ipsum lorem ipsum lorem ipsum lorem ipsum dolor sit lorom ipsum doler sit 
                    lorem ipsum lorem ipsum lorem ipsum lorem ipsum dolor sit lorom ipsum doler sit 
                    lorem ipsum lorem ipsum lorem ipsum lorem ipsum dolor sit lorom ipsum doler sit 
                </p>
            </div>
        </div>
    )
}

export default TestimonialCard
