import React, { Component } from 'react';
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./TestimonialCard.css";
import img1 from "../../images/testimonial1.jpeg";
import img2 from "../../images/testimonial2.jpeg";
import img3 from "../../images/testimonial3.jpeg";
// import cardData from "./cardData"

const Cards = ({cardData}) =>{
    console.log(cardData);
    return(
        <>
        <div className="container">
        <div className="row">
        <div className="col-md-8 col-center m-auto">
            <h2>Testimonials</h2>
            <div id="myCarousel"  className="carousel slide" data-ride="carousel" > 
                <div className="carousel-inner">
                    {cardData.map((slide , index )=>{
                        return(
                            <div className="item carousel-item active" key={index} >
                            <div className="img-box"> 
                            <img src={slide.image} alt={slide.alt}/>
                            </div>
                            <p className="testimonial">
                                {slide.testimonial}
                            </p>
                            <p className="overview">
                                <b> {slide.name} </b> ,<span>{slide.designation}</span> 
                            </p>
                            </div> 
                        )
                    })}
                    {/* <div className="item carousel-item active" >
                    <div className="img-box"> 
                    <img src={img1} alt="profilepic1"/>
                    </div>
                    <p className="testimonial">
                        Ezerx Health Tech Pvt. Ltd , a BIG grantee of BIRAC @KKIT TBI developing non invassive device for 
                        haemoglobin, bilirubin and SPO2. Delivered 1st order 42 lakhs. Also developing detection for oral cancer
                    </p>
                    <p className="overview">
                        <b> Dr. Mrutyunjay Suar </b> ,<span>Scientific Advisor</span> 
                    </p>
                    </div>
                    <div className="item carousel-item ">
                    <div className="img-box"> 
                    <img src={img2} alt="profilepic1"/>
                    </div>
                    <p className="testimonial">
                        This is great Lorem Lorem Lore m kjsfbhjasfbnz vxmn
                        This is great Lorem Lorem Lore m kjsfbhjasfbnz vxmn
                        This is great Lorem Lorem Lore m kjsfbhjasfbnz vxmn
                         
                    </p>
                    <p className="overview">
                        <b> Ram Ratan Jagadeb </b> , Scientific Advisor
                    </p>
                    </div>
                    <div className="item carousel-item ">
                    <div className="img-box"> 
                    <img src={img3} alt="profilepic1"/>
                    </div>
                    <p className="testimonial">
                        Lorem Lorem Lorem ipsum lorem ipsum 
                        Lorem Lorem Lorem ipsum lorem ipsum 
                        Lorem Lorem Lorem ipsum lorem ipsum 
                        Lorem Lorem Lorem ipsum lorem ipsum 
                    </p>
                    <p className="overview">
                        <b> Sambit Mahpatra </b> , Scientific Advisor
                    </p>
                    </div> */}
            </div>
            {/* <a className="carousel-control left carousel-control-prev" href="myCarousel" data-slide="prev">   
            <i className="fa fa-angle-left"></i>
            </a>
            <a className="carousel-control right carousel-control-next" href="myCarousel" data-slide="next">   
            <i className="fa fa-angle-right"></i>
            </a> */}
        </div>
        </div>
        </div>
        </div>
        </>
    )
}

export default Cards

 {/* <div className="item carousel-item active" >
                    <div className="img-box"> 
                    <img src={img1} alt="profilepic1"/>
                    </div>
                    <p className="testimonial">
                        Ezerx Health Tech Pvt. Ltd , a BIG grantee of BIRAC @KKIT TBI developing non invassive device for 
                        haemoglobin, bilirubin and SPO2. Delivered 1st order 42 lakhs. Also developing detection for oral cancer
                    </p>
                    <p className="overview">
                        <b> Dr. Mrutyunjay Suar </b> ,<span>Scientific Advisor</span> 
                    </p>
                    </div>
                    <div className="item carousel-item ">
                    <div className="img-box"> 
                    <img src={img2} alt="profilepic1"/>
                    </div>
                    <p className="testimonial">
                        This is great Lorem Lorem Lore m kjsfbhjasfbnz vxmn
                        This is great Lorem Lorem Lore m kjsfbhjasfbnz vxmn
                        This is great Lorem Lorem Lore m kjsfbhjasfbnz vxmn
                         
                    </p>
                    <p className="overview">
                        <b> Ram Ratan Jagadeb </b> , Scientific Advisor
                    </p>
                    </div>
                    <div className="item carousel-item ">
                    <div className="img-box"> 
                    <img src={img3} alt="profilepic1"/>
                    </div>
                    <p className="testimonial">
                        Lorem Lorem Lorem ipsum lorem ipsum 
                        Lorem Lorem Lorem ipsum lorem ipsum 
                        Lorem Lorem Lorem ipsum lorem ipsum 
                        Lorem Lorem Lorem ipsum lorem ipsum 
                    </p>
                    <p className="overview">
                        <b> Sambit Mahpatra </b> , Scientific Advisor
                    </p>
                    </div> */}