import React , {Component}from 'react';
import './App.css';
import { BrowserRouter as Router , Switch , Route} from "react-router-dom" ;
import { InfoBar } from "./components/InfoBar";
import {Home} from "./components/Home";
import {Media} from "./components/Media/Media";
import NavbarTwo from "./components/Navbartwo/Navbartwo"
import Dropdown from "./components/Navbartwo/Dropdown"
import {Foot} from "./components/FooterTwo/Foot";
import Ezecheck from "./components/Ezecheckpage/Ezecheck"

class App extends Component {
  render(){
  return (
    <>
      <Router>
        <InfoBar/>
        <NavbarTwo/>
        {/* <Dropdown/> */}
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/media" component={Media} />
          <Route exact path="/ezecheck" component={Ezecheck} />
        </Switch>
        <Foot/>
      </Router>
    </>
  );
      }
}

export default App;
